package com.plaiifah.hello

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.plaiifah.hello.databinding.ActivityHelloBinding

class ActivityHello : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.newname.text = intent.getStringExtra("key")
    }
}
