package com.plaiifah.hello

import android.content.ContentValues.TAG
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.plaiifah.hello.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btn.setOnClickListener{
            Log.d(TAG, " "+binding.name.text)
            Log.d(TAG, " "+binding.code.text)
            onClick()
        }

    }
    fun onClick(){
        val intent = Intent(this, ActivityHello ::class.java)
        intent.putExtra("key",binding.name.text)
        startActivity(intent)
    }


}